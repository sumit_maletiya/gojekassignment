package com.assignment.gojek.framework.db


interface LocalDataHelper {
    fun insertData(repositoryEntity: RepositoryEntity)
    fun getAllRepositories(): List<RepositoryEntity>
}