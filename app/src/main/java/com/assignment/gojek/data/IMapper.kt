package com.assignment.gojek.data

interface IMapper<Entity, Model> {

    fun mapToModel(entity: Entity): Model



    fun mapListToModel(entities: List<Entity>): List<Model> {
        val list = mutableListOf<Model>()
        entities.mapTo(list) { mapToModel(it) }
        return list
    }

}