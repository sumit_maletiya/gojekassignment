package com.assignment.gojek.framework.db

import android.content.SharedPreferences
import javax.inject.Inject

class SharedPref @Inject constructor(val sharedPreference: SharedPreferences) {
    companion object {
         const val PRIVATE_MODE = 0
         const val PREF_NAME = "gojek_data"
         const val LAST_TIME_STAMP = "last_time_stemp"

    }
    fun getLastTimeStemp(): Long = sharedPreference.getLong(LAST_TIME_STAMP, 0)
    fun setLastTimeStemp(timeStemp: Long) {
        val editor = sharedPreference.edit()
        editor.putLong(LAST_TIME_STAMP, timeStemp).apply()
    }

}