package com.assignment.gojek.domain


data class RepositoriesModel(
    var author: String?,
    var avatar: String?,
    var description: String?,
    var forks: Int,
    var language: String?,
    var name: String?,
    var stars: Int,
    var url: String?,
    var isExpended:Boolean
)