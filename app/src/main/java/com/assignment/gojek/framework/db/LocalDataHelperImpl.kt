package com.assignment.gojek.framework.db

import android.content.Context
import com.db.RepositoryDatabase
import javax.inject.Inject

class LocalDataHelperImpl @Inject constructor(context: Context) : LocalDataHelper {


    val db: RepositoryDatabase by lazy {
        RepositoryDatabase.getDatabase(context)
    }

    override fun insertData(repositoryEntity: RepositoryEntity) {
        repositoryEntity.apply {
            db.repositoryDao().insertRepo(repositoryEntity)
        }

    }

    override fun getAllRepositories(): List<RepositoryEntity> {
        return db.repositoryDao().getAllRepo()
    }

}