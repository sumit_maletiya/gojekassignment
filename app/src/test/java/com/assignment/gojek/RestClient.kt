package com.assignment.gojek

import com.apis.ApiServices
import com.squareup.okhttp.OkHttpClient
import retrofit.JacksonConverterFactory
import retrofit.Retrofit
import javax.inject.Inject


object RestClient {
    @Inject
    private var mRestService: ApiServices? = null

    val repositories: ApiServices?
        get() {
            if (mRestService == null) {
                val client = OkHttpClient()
                client.interceptors().add(FakeInterceptor())

                val retrofit = Retrofit.Builder()
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl("https://github-trending-api.now.sh")
                    .client(client)
                    .build()

                mRestService = retrofit.create(ApiServices::class.java)
            }
            return mRestService
        }
}