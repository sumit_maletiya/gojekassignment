package com.assignment.gojek

import com.squareup.okhttp.*
import java.io.IOException


class FakeInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        var response: Response? = null
        if (BuildConfig.DEBUG) {
            val responseString: String
            // Get Request URI.
            val uri = chain.request().uri()
            // Get Query String.
            val query = uri.query
            // Parse the Query String.
            val parsedQuery =
                query.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            responseString = if (parsedQuery[0].equals("id", ignoreCase = true) && parsedQuery[1].equals("1",ignoreCase = true)
            ) {
                REPOSITORY_RESPONSE1
            } else if (parsedQuery[0].equals("id", ignoreCase = true) && parsedQuery[1].equals(
                    "2",
                    ignoreCase = true
                )
            ) {
                REPOSITORY_RESPONSE2
            } else {
                ""
            }

            response = Response.Builder()
                .code(200)
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_0)
                .body(
                    ResponseBody.create(
                        MediaType.parse("application/json"),
                        responseString.toByteArray()
                    )
                )
                .addHeader("content-type", "application/json")
                .build()

        } else {
            response = chain.proceed(chain.request())
        }

        return response
    }

    companion object {
        // FAKE RESPONSES.
        private val REPOSITORY_RESPONSE1 = "   {\n" +
                "        \"author\": \"allthemusicllc\",\n" +
                "        \"name\": \"atm-cli\",\n" +
                "        \"avatar\": \"https://github.com/allthemusicllc.png\",\n" +
                "        \"url\": \"https://github.com/allthemusicllc/atm-cli\",\n" +
                "        \"description\": \"Command line tool for generating and working with MIDI files.\",\n" +
                "        \"language\": \"Rust\",\n" +
                "        \"languageColor\": \"#dea584\",\n" +
                "        \"stars\": 657,\n" +
                "        \"forks\": 0,\n" +
                "        \"currentPeriodStars\": 171,\n" +
                "        \"builtBy\": [\n" +
                "            {\n" +
                "                \"username\": \"allthemusicllc\",\n" +
                "                \"href\": \"https://github.com/allthemusicllc\",\n" +
                "                \"avatar\": \"https://avatars1.githubusercontent.com/u/53976615\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }"
        private val REPOSITORY_RESPONSE2 = "{\n" +
                "        \"author\": \"jwasham\",\n" +
                "        \"name\": \"coding-interview-university\",\n" +
                "        \"avatar\": \"https://github.com/jwasham.png\",\n" +
                "        \"url\": \"https://github.com/jwasham/coding-interview-university\",\n" +
                "        \"description\": \"A complete computer science study plan to become a software engineer.\",\n" +
                "        \"stars\": 99962,\n" +
                "        \"forks\": 0,\n" +
                "        \"currentPeriodStars\": 498,\n" +
                "        \"builtBy\": [\n" +
                "            {\n" +
                "                \"username\": \"jwasham\",\n" +
                "                \"href\": \"https://github.com/jwasham\",\n" +
                "                \"avatar\": \"https://avatars0.githubusercontent.com/u/3771963\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"username\": \"avizmarlon\",\n" +
                "                \"href\": \"https://github.com/avizmarlon\",\n" +
                "                \"avatar\": \"https://avatars3.githubusercontent.com/u/24354489\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"username\": \"YoSaucedo\",\n" +
                "                \"href\": \"https://github.com/YoSaucedo\",\n" +
                "                \"avatar\": \"https://avatars0.githubusercontent.com/u/13459501\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"username\": \"aleen42\",\n" +
                "                \"href\": \"https://github.com/aleen42\",\n" +
                "                \"avatar\": \"https://avatars3.githubusercontent.com/u/9573300\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"username\": \"strollkim\",\n" +
                "                \"href\": \"https://github.com/strollkim\",\n" +
                "                \"avatar\": \"https://avatars2.githubusercontent.com/u/14087177\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }"
    }
}