package com.assignment.gojek.domain

interface IInternetStatus {
    val isConnected: Boolean
}