package com.assignment.gojek

import android.content.Context
import android.content.SharedPreferences
import com.assignment.gojek.data.FetchRepository
import com.assignment.gojek.domain.InternetStatusImpl
import com.assignment.gojek.domain.RepositoriesModel
import com.assignment.gojek.framework.db.SharedPref
import com.assignment.gojek.presentation.AssignmentApp
import com.assignment.gojek.presentation.viewmodel.MainViewModel
import com.assignment.gojek.usecases.GetRepositories
import com.assignment.gojek.usecases.Instructors
import com.assignment.gojek.usecases.StoreDataLocally
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MainActivityUnitTest {
    @Mock
    lateinit var application: AssignmentApp

    private lateinit var viewModel: MainViewModel

    private var repositoriesModel: MutableList<RepositoriesModel>? = null
    private val filePath =
        "D:\\sumit\\android_workspace\\gojekassignment\\app\\mokefiles\\api_response.json"

    init {
        val listType = object : TypeToken<MutableList<RepositoriesModel>>() {

        }.type
        repositoriesModel = Gson().fromJson<MutableList<RepositoriesModel>>(
            readFile(filePath),
            listType
        )
    }

    @Before
    fun setUp() {
        val sharedPrefs = mock(SharedPreferences::class.java)
        val context = mock(Context::class.java)
        val mInternetStatus =InternetStatusImpl(context)
        val repository = FetchRepository(mInternetStatus)
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(application, Instructors(GetRepositories(repository),
            StoreDataLocally(repository)
        ), mInternetStatus, SharedPref(sharedPrefs)
        )
    }

    @Test
    fun isSortByStarCorrect() {
        var sortedRepo: MutableList<RepositoriesModel>? = null
        repositoriesModel?.let { it ->
            sortedRepo = viewModel.sortByStars(it)
        }
        sortedRepo?.let {
            assertTrue(it[1].stars < it[2].stars)
            assertTrue(it[5].stars < it[6].stars)
        }
    }
    @Test
    fun isSortByNameCorrect() {
        var sortedRepo: MutableList<RepositoriesModel>? = null
        repositoriesModel?.let { it ->
            sortedRepo = viewModel.sortByName(it)
            sortedRepo?.let {sortedList->
                assertTrue(sortedList[1].name?.startsWith('A',true)==true)
            }
        }

    }
    @Test
    fun lastUpdateMsg_positive_1to30() {
        //positive case for first condition.
        val minutes:Long= 25*60*1000
        viewModel.setLastUpdateMessage(minutes)
        assertEquals(viewModel.lastUpdateMsg.get(),"Updated 25 mins ago")

    }
    @Test
    fun lastUpdateMsg_positive_30to1() {
        //positive case for first condition.
        val minutes:Long= 60*60*1000
        viewModel.setLastUpdateMessage(minutes)
        assertEquals(viewModel.lastUpdateMsg.get(),"Updated about 30 mins ago")
    }
    @Test
    fun lastUpdateMsg_positive_1to2() {
        //positive case for first condition.
        val minutes:Long= 1*80*60*1000
        viewModel.setLastUpdateMessage(minutes)
        assertEquals(viewModel.lastUpdateMsg.get(),"Updated about 1 hour ago")
    }
    @After
    @Throws(Exception::class)
    fun tearDown() {
    }
}
