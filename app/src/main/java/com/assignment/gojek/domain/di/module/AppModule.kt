package com.assignment.gojek.domain.di.module

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.assignment.gojek.framework.db.SharedPref

import com.assignment.gojek.presentation.AssignmentApp
import com.google.gson.Gson
import com.google.gson.GsonBuilder

import javax.inject.Named
import javax.inject.Singleton

import dagger.Module
import dagger.Provides

@Module
class AppModule {
    val gson: Gson
        @Singleton
        @Provides
        get() = GsonBuilder().create()

    @Singleton
    @Provides
    fun appContext(): Context {
        return AssignmentApp.application.applicationContext
    }
    @Singleton
    @Provides
    fun getPreference(context:Context): SharedPref {
        return SharedPref(context.getSharedPreferences(SharedPref.PREF_NAME,
            SharedPref.PRIVATE_MODE))
    }
}
