package com.assignment.gojek.presentation.Exceptions

class NoDataException : Exception {
    constructor(message: String) : super(message)
}