package com.assignment.gojek.usecases

/**
 * responsible to get all business use cases
 */
data class Instructors(
    var repositories: GetRepositories,
    var storeDataLocally: StoreDataLocally
)
