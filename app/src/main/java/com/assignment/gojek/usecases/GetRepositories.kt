package com.assignment.gojek.usecases

import com.assignment.gojek.data.FetchRepository

/**
 * Get repositories list from remote server or database
 */
class GetRepositories(private val photoSearchRepository: FetchRepository) {
    operator fun invoke()
            = photoSearchRepository.getRepository().toObservable()
}