package com.assignment.gojek.domain.di.module

import android.content.Context

import com.apis.ApiServices
import com.assignment.gojek.framework.db.LocalDataHelper
import com.assignment.gojek.framework.db.LocalDataHelperImpl
import com.assignment.gojek.data.FetchRepository
import com.assignment.gojek.usecases.GetRepositories
import com.assignment.gojek.usecases.Instructors
import com.assignment.gojek.usecases.StoreDataLocally
import com.assignment.gojek.domain.di.scope.ActivityScope

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ViewModelProviderModule {


    @Singleton
    @Provides
    fun provideLocalDataHelper(context: Context): LocalDataHelper {
        return LocalDataHelperImpl(context)
    }

    @ActivityScope
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiServices {
        return retrofit.create(ApiServices::class.java)
    }
    
    @ActivityScope
    @Provides
    fun provideIntractor(searchRepo: FetchRepository): Instructors {
        return Instructors(GetRepositories(searchRepo), StoreDataLocally(searchRepo))
    }
}

