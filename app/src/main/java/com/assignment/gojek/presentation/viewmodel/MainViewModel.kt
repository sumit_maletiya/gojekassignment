package com.assignment.gojek.presentation.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.assignment.gojek.domain.IInternetStatus
import com.assignment.gojek.domain.RepositoriesModel
import com.assignment.gojek.usecases.Instructors
import com.assignment.gojek.framework.db.RepositoryEntity
import com.assignment.gojek.framework.db.SharedPref
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class MainViewModel constructor(
    application: Application, val intractors: Instructors, val mInternetStatus: IInternetStatus,
    val mSharedPref: SharedPref
) : BaseViewModel(application, intractors, mInternetStatus) {
    val isLoading = ObservableField<Boolean>()
    val isError = ObservableField<Boolean>()
    val isFetchData = ObservableField<Boolean>()
    private val cacheExpiredTime = 2 * 60 * 60 * 1000
    var lastUpdateMsg = ObservableField<String>()
    private val mCompositeDisposable by lazy {
        CompositeDisposable()
    }

    val repoResponseData = MutableLiveData<List<RepositoryEntity>>()

    fun getRepositories() {
        intractors.repositories()
            .subscribe(object : Observer<List<RepositoryEntity>> {
                override fun onSubscribe(d: Disposable) {
                    mCompositeDisposable.add(d)
                }

                override fun onNext(repositoriesModel: List<RepositoryEntity>) {
                    if (repositoriesModel.isNotEmpty()) {
                        isFetchData.set(true)
                        isError.set(false)
                        isLoading.set(false)
                        repoResponseData.postValue(repositoriesModel)
                        setLastUpdateMessage(getTimeDifference())
                        if (mInternetStatus.isConnected && getTimeDifference() > cacheExpiredTime
                        ) {
                            intractors.storeDataLocally(repositoriesModel).subscribe()
                            mSharedPref.setLastTimeStemp(System.currentTimeMillis())
                        }
                    } else {
                        isFetchData.set(false)
                        isError.set(true)
                    }
                }

                override fun onError(e: Throwable) {
                    isFetchData.set(false)
                    isError.set(true)
                }

                override fun onComplete() {

                }
            })
    }

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun callRetry() {
        isError.set(false)
        getRepositories()
    }

    fun sortByStars(mRepoItems: MutableList<RepositoriesModel>): MutableList<RepositoriesModel> {
        return mRepoItems.sortedWith(compareBy { it.stars }).toMutableList()
    }

    fun sortByName(mRepoItems: MutableList<RepositoriesModel>): MutableList<RepositoriesModel> {
        return mRepoItems.sortedWith(compareBy { it.name?.toLowerCase() }).toMutableList()
    }

    fun setLastUpdateMessage(timeDifference: Long) {
        val oneMin = 60 * 1000
        if (timeDifference > oneMin && timeDifference <= (oneMin * 30)) {
            val minutes: Int = (timeDifference / oneMin).toInt()
            lastUpdateMsg.set("Updated $minutes mins ago")
        } else if (timeDifference > (oneMin * 30) && timeDifference <= (oneMin * 60)) {
            lastUpdateMsg.set("Updated about 30 mins ago")
        } else if (timeDifference > (oneMin * 60) && timeDifference < cacheExpiredTime) {
            lastUpdateMsg.set("Updated about 1 hour ago")
        }
    }

    fun getTimeDifference(): Long {
        return System.currentTimeMillis() - mSharedPref.getLastTimeStemp()
    }

    fun onRefresh() {
        isLoading.set(true)
        getRepositories()
    }

}