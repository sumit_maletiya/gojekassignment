package com.assignment.gojek.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "repositories")
data class RepositoryEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "name")
    @SerializedName("name")
    val name: String?,
    @ColumnInfo(name = "author")
    @SerializedName("author")
    val author: String?,
    @ColumnInfo(name = "repoAvatar")
    @SerializedName("avatar")
    val avatar: String?,
    @ColumnInfo(name = "url")
    @SerializedName("url")
    val resourceURI: String?,
    @ColumnInfo(name = "language")
    @SerializedName("language")
    val language: String?,
    @ColumnInfo(name = "stars")
    @SerializedName("stars")
    val stars: Int,
    @ColumnInfo(name = "forks")
    @SerializedName("forks")
    val forks: Int,
    @ColumnInfo(name = "description")
    @SerializedName("description")
    val description: String?
)