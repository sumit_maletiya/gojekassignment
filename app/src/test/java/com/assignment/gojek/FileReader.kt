package com.assignment.gojek

import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.lang.StringBuilder

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
fun readFile(fileName: String): String {
    val stringBuilder = StringBuilder()
    try {
        val lines: List<String> = File(fileName).readLines()
        lines.forEach {
                line -> stringBuilder.append(line)
        }
    } catch (e:IOException) {
        e.printStackTrace()
    } finally {
       return stringBuilder.toString()
    }
}
