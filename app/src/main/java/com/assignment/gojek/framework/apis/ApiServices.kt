package com.apis

import com.assignment.gojek.framework.db.RepositoryEntity
import io.reactivex.Single
import retrofit2.http.GET

interface ApiServices {
        @GET("/repositories")
        fun getRepositories(): Single<List<RepositoryEntity>>
}
