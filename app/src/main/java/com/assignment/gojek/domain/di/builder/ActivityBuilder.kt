package com.assignment.gojek.domain.di.builder

import com.assignment.gojek.presentation.MainActivity
import com.assignment.gojek.domain.di.module.ViewModelProviderModule
import com.assignment.gojek.domain.di.scope.ActivityScope

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector(modules = [ViewModelProviderModule::class])
    abstract fun bindMainActivity(): MainActivity
}
