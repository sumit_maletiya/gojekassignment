package com.assignment.gojek.domain.di.component

import com.assignment.gojek.presentation.AssignmentApp
import com.assignment.gojek.domain.di.builder.ActivityBuilder
import com.assignment.gojek.domain.di.module.ApiModule
import com.assignment.gojek.domain.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ApiModule::class, ActivityBuilder::class])
interface AppComponent {

    fun inject(application: AssignmentApp)

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun application(application: AssignmentApp): Builder
    }
}
