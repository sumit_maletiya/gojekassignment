package com.assignment.gojek.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.assignment.gojek.domain.IInternetStatus
import com.assignment.gojek.presentation.AssignmentApp
import com.assignment.gojek.usecases.Instructors

open class BaseViewModel protected constructor(application: Application,
                                               intractors: Instructors, mInternetStatus: IInternetStatus
) : AndroidViewModel(application) {
    protected val application: AssignmentApp = getApplication()
}