package com.assignment.gojek.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.assignment.gojek.domain.IInternetStatus
import com.assignment.gojek.framework.db.SharedPref
import com.assignment.gojek.usecases.Instructors
import javax.inject.Inject

class ViewModelFactory @Inject constructor(
    val application: Application, val intractors: Instructors,
    private val internetStatus: IInternetStatus, private val pref: SharedPref
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (BaseViewModel::class.java.isAssignableFrom(modelClass)) {
            return modelClass.getConstructor(
                Application::class.java,
                Instructors::class.java, IInternetStatus::class.java, SharedPref::class.java
            )
                .newInstance(application, intractors, internetStatus, pref)
        } else {
            throw IllegalArgumentException("unexpected viewModel class $modelClass")
        }
    }
}