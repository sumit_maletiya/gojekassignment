package com.assignment.gojek.usecases

import com.assignment.gojek.data.FetchRepository
import com.assignment.gojek.framework.db.RepositoryEntity

/**
 * Store data locally
 */
class StoreDataLocally(private val photoSearchRepository: FetchRepository) {
    operator fun invoke(repositoryEntities: List<RepositoryEntity>) =
        photoSearchRepository.storeDataLocallyObservable(
            repositories = repositoryEntities
        )
}