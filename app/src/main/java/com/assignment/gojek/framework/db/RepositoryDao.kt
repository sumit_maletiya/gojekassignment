package com.assignment.gojek.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface RepositoryDao {

    @Insert(onConflict = REPLACE)
    fun insertRepo(flicker: RepositoryEntity)

    @Query("SELECT * FROM repositories")
    fun getAllRepo(): List<RepositoryEntity>
}