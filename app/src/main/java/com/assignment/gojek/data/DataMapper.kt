package com.assignment.gojek.data

import com.assignment.gojek.domain.RepositoriesModel
import com.assignment.gojek.framework.db.RepositoryEntity

/**
 * To map entity to repositoriesModel
 * @entity RepositoryEntity
 */
class DataMapper:IMapper<RepositoryEntity,RepositoriesModel>{
    override fun mapToModel(entity: RepositoryEntity): RepositoriesModel {
         return RepositoriesModel(
            name = entity.name,
            author = entity.author,
            avatar = entity.avatar,
            url = entity.resourceURI,
            language = entity.language,
            stars = entity.stars,
            forks = entity.forks,
            description = entity.description,
            isExpended = false
        )
    }

}