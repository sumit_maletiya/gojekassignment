package com.assignment.gojek.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.assignment.gojek.BR
import com.assignment.gojek.R
import com.assignment.gojek.data.DataMapper
import com.assignment.gojek.databinding.ActivityMainBinding
import com.assignment.gojek.domain.InternetStatusImpl
import com.assignment.gojek.domain.RepositoriesModel
import com.assignment.gojek.presentation.activity.BaseActivity
import com.assignment.gojek.presentation.viewmodel.MainViewModel
import com.assignment.gojek.presentation.viewmodel.ViewModelFactory
import com.assignment.gojek.presentation.adapters.RepositoryAdapter
import com.assignment.gojek.usecases.Instructors
import com.assignment.gojek.framework.db.RepositoryEntity
import com.assignment.gojek.framework.db.SharedPref
import com.assignment.gojek.presentation.adapters.VerticalSpaceItemDecoration
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_place_holder.*
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    @Inject
    lateinit var intractors: Instructors
    @Inject
    lateinit var internetStatusImpl: InternetStatusImpl
    @Inject
    lateinit var pref: SharedPref
    private var mRepositoryAdapter: RepositoryAdapter? = null
    private var mRepoItems = mutableListOf<RepositoriesModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel?.getRepositories()
        shimmer_view_container.startShimmerAnimation()
        handleLiveData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.option_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_sortByStars -> {
                viewModel?.sortByStars(mRepoItems)?.let {
                    mRepoItems.clear()
                    mRepoItems.addAll(it)
                    mRepositoryAdapter?.notifyDataSetChanged()
                }
            }
            R.id.menu_sortByName -> {
                viewModel?.sortByName(mRepoItems)?.let {
                    mRepoItems.clear()
                    mRepoItems.addAll(it)
                    mRepositoryAdapter?.notifyDataSetChanged()
                }

            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun initializeViews(bundle: Bundle?) {
        AndroidInjection.inject(this)
        if (supportActionBar != null) {
            supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
            supportActionBar?.setCustomView(R.layout.action_bar)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(false)
        }
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        rvRepositories?.layoutManager = linearLayoutManager
        mRepositoryAdapter = RepositoryAdapter(mRepoItems)
        rvRepositories?.adapter = mRepositoryAdapter
        rvRepositories?.addItemDecoration(VerticalSpaceItemDecoration(3))
        (rvRepositories.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rvRepositories?.addItemDecoration(
            DividerItemDecoration(
                rvRepositories.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun initViewModel(): MainViewModel {
        val viewModelFactory = ViewModelFactory(
            this.application,
            intractors, internetStatusImpl, pref
        )
        return ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun getBindingVariable(): Int {
        return BR.viewmodel
    }

    private fun handleLiveData() {
        viewModel?.repoResponseData?.observe(this,
            Observer<List<RepositoryEntity>> { repoItems ->
                repoItems.let {
                    if (repoItems.isEmpty()) {
                        Toast.makeText(
                            this@MainActivity,
                            getString(R.string.no_data_found),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    mRepoItems.clear()
                    if (repoItems != null) {
                        shimmer_view_container.stopShimmerAnimation()
                        mRepoItems.addAll(DataMapper().mapListToModel(it))
                        mRepositoryAdapter?.notifyDataSetChanged()
                    }
                }
            })
    }


}
