package com.assignment.gojek.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.assignment.gojek.R
import com.assignment.gojek.databinding.RepositoryItemBinding
import com.assignment.gojek.domain.RepositoriesModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.repository_item.view.*


class RepositoryAdapter(
    private val mRepoItem: MutableList<RepositoriesModel>
) : RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder>() {
    private var previousSelectedPos = -1
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RepositoryViewHolder {
        val binding = DataBindingUtil.inflate<RepositoryItemBinding>(
            LayoutInflater.from(
                viewGroup.context
            ), R.layout.repository_item, viewGroup, false
        )
        return RepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(photoGalleryViewHolder: RepositoryViewHolder, position: Int) {
        if (position != previousSelectedPos) mRepoItem[position].isExpended = false
        photoGalleryViewHolder.itemView.rv_parentView.setOnClickListener {
            mRepoItem[position].isExpended = !mRepoItem[position].isExpended
            previousSelectedPos = position
            notifyDataSetChanged()
        }
        return photoGalleryViewHolder.bind(mRepoItem[position])
    }


    override fun getItemCount(): Int {
        return mRepoItem.size
    }

    inner class RepositoryViewHolder(itemView: RepositoryItemBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        private val viewBinding by lazy {
            itemView
        }

        fun bind(item: RepositoriesModel) {
            Glide.with(itemView.imageView.context)
                .setDefaultRequestOptions(RequestOptions().circleCrop())
                .load(item.avatar)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_error_outline_black_24dp)
                .into(itemView.imageView)
            itemView.lv_subItem.visibility = if (item.isExpended) View.VISIBLE else View.GONE
            viewBinding.setVariable(BR.repository, item)
            viewBinding.executePendingBindings()
        }
    }
}